﻿using UnityEngine;
using UnityEngine.UI;

public class DisplayScore : MonoBehaviour
{
    private GameSession gameSession;
    private Text scoreText;
    
    // Start is called before the first frame update
    void Start()
    {
        gameSession = FindObjectOfType<GameSession>();
        scoreText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        var score = gameSession.GetScore();
        scoreText.text = score.ToString();
    }
}
