﻿using UnityEngine;

public class BackgroundScroller : MonoBehaviour
{
    [SerializeField] private float scrollSpeed = 0.02f;
    
    private Material material;
    
    void Start()
    {
        material = GetComponent<Renderer>().material;
    }

    void Update()
    {
        var scrollOffset = new Vector2(0, scrollSpeed);
        material.mainTextureOffset += scrollOffset * Time.deltaTime;
    }
}
