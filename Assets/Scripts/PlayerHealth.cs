﻿public class PlayerHealth : Health
{
    protected override void Die()
    {
        base.Die();
        FindObjectOfType<SceneLoader>().LoadGameOver();
    }
}
