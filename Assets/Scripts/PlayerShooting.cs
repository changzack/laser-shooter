﻿using System.Collections;
using UnityEngine;

public class PlayerShooting: MonoBehaviour, IShooting
{
    [SerializeField] private GameObject laserPrefab;
    [SerializeField] private float laserSpeed = 10f;
    [SerializeField] private float fireInterval = 0.1f;
    [SerializeField] private AudioClip shootSound;
    [SerializeField] [Range(0, 1)] private float shootVolume = 0.1f;
    
    private Coroutine fireCoroutine;
    private Camera camera1;

    private void Start()
    {
        Debug.Assert(Camera.main != null, "Camera.main != null");
    }

    public void Fire()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            fireCoroutine = StartCoroutine(StartFireLoop());
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            StopCoroutine(fireCoroutine);
        }
    }
    
    private IEnumerator StartFireLoop()
    {
        while (true)
        {
            MakeLaserAndSound();
            yield return new WaitForSeconds(fireInterval);
        }
    }

    private void MakeLaserAndSound()
    {
        var laser = Instantiate(laserPrefab, transform.position, Quaternion.identity);
        laser.GetComponent<Rigidbody2D>().velocity = new Vector2(0, laserSpeed);
        AudioSource.PlayClipAtPoint(shootSound, Camera.main.transform.position, shootVolume);
    }
}