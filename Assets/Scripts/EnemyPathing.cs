﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPathing : MonoBehaviour
{
    private WaveConfig waveConfig;

    private List<Vector2> wayPoints;
    private int moveIndex = 0;

    void Start()
    {
        wayPoints = waveConfig.GetWayPoints();
        InitPosition();
    }

    public void SetWaveConfig(WaveConfig waveConfig)
    {
        this.waveConfig = waveConfig;
    }

    private void InitPosition()
    {
        transform.position = wayPoints[moveIndex];
    }

    void Update()
    {
        Move();
    }

    private void Move()
    {
        if (moveIndex < wayPoints.Count)
        {
            var deltaSpeed = waveConfig.GetMoveSpeed() * Time.deltaTime;
            transform.position = Vector2.MoveTowards(transform.position, 
                                                      wayPoints[moveIndex], deltaSpeed);
            CheckMoveIndex();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void CheckMoveIndex()
    {
        if ((Vector2)transform.position == wayPoints[moveIndex])
        {
            moveIndex++;
        }
    }
}
