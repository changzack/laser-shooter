﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy Wave Config")]
public class WaveConfig : ScriptableObject
{
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private GameObject pathPrefab;
    [SerializeField] private int numberOfEnemies = 5;
    [SerializeField] private float timeBetweenSpawns = 0.25f;
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private float spawnRandomFactor = 0.3f;
    
    public GameObject GetEnemyPrefab(){ return enemyPrefab; }

    public List<Vector2> GetWayPoints()
    {
        var wayPoints = new List<Vector2>();
        foreach (Transform child in pathPrefab.transform)
        {
                wayPoints.Add(child.position);
        }
        return wayPoints;
    }
    public int GetNumberOfEnemies(){ return numberOfEnemies; }
    public float GetTimeBetweenSpawns(){ return timeBetweenSpawns; }
    public float GetMoveSpeed(){ return moveSpeed; }
    public float GetSpawnRandomFactor(){ return spawnRandomFactor; }
}
