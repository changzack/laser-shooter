﻿using UnityEngine;
using UnityEngine.UI;

public class DisplayHealth : MonoBehaviour
{
    private Health playerHealth;
    private Text healthText;
    
    // Start is called before the first frame update
    void Start()
    {
        playerHealth = FindObjectOfType<Player>().GetComponent<Health>();
        healthText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        var health = playerHealth.GetHealth();
        healthText.text = health.ToString();
    }
}
