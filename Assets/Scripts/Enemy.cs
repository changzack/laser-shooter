﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private int scoreValue = 100;
    
    private IShooting shooting;

    private void Start()
    {
        shooting = GetComponent<IShooting>();
        Debug.Assert(shooting != null, "shooting != null");
    }

    private void Update()
    {
        shooting.Fire();
    }

    public int GetScoreValue()
    {
        return scoreValue;
    }
}
