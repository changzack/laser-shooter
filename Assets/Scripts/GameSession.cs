﻿using UnityEngine;

public class GameSession : MonoBehaviour
{
    [SerializeField] private int score;
    
    void Awake()
    {
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            GameObject o;
            (o = gameObject).SetActive(false);
            Destroy(o);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    public int GetScore()
    {
        return score;
    }

    public void AddScore(int addValue)
    {
        score += addValue;
    }
    
    public void ResetScore()
    {
        score = 0;
    }
}
