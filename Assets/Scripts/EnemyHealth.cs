﻿public class EnemyHealth: Health
{
    protected override void Die()
    {
        var scoreValue = GetComponent<Enemy>().GetScoreValue();
        FindObjectOfType<GameSession>().AddScore(scoreValue);
        base.Die();
    }
}