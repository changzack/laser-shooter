﻿using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private int health = 200;
    [SerializeField] private GameObject explosionVfxPrefab;
    [SerializeField] private float explosionDuration = 1f;
    [SerializeField] private AudioClip deathSound;
    [SerializeField] [Range(0, 1)] private float deathVolume = 0.75f;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        var damageDealer = other.GetComponent<DamageDealer>();
        if (!damageDealer) 
            return;
        ProcessDamage(damageDealer);
    }

    private void ProcessDamage(DamageDealer damageDealer)
    {
        health -= damageDealer.GetDamage();
        damageDealer.Hit();
        if (health <= 0)
        {
            Die();
        }
    }

    protected virtual void Die()
    {
        PlayVfx();
        PlaySfx();
        Destroy(gameObject);
    }

    private void PlaySfx()
    {
        if (Camera.main != null)
            AudioSource.PlayClipAtPoint(deathSound, Camera.main.transform.position, deathVolume);
    }

    private void PlayVfx()
    {
        var vfx = Instantiate(explosionVfxPrefab, transform.position, Quaternion.identity);
        Destroy(vfx, explosionDuration);
    }

    public int GetHealth()
    {
        return health;
    }
}
