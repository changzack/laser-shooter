﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveLauncher : MonoBehaviour
{
    [SerializeField] 
    private List<WaveConfig> waveConfigs;
    [SerializeField]
    private bool isLoop = false;
    
    private int startWaveIndex = 0;
    
    // Start is called before the first frame update
    IEnumerator Start()
    {
        do
        {
            yield return StartCoroutine(StartWaves());
        } while (isLoop);
    }

    private IEnumerator StartWaves()
    {
        for (int i = 0; i < waveConfigs.Count; i++)
        {
            yield return StartCoroutine(LaunchEnemyWave(waveConfigs[i]));
        }
    }
    
    private IEnumerator LaunchEnemyWave(WaveConfig waveConfig)
    {
        for (int i = 0; i < waveConfig.GetNumberOfEnemies(); i++)
        {
            var enemy = Instantiate(waveConfig.GetEnemyPrefab());
            enemy.GetComponent<EnemyPathing>().SetWaveConfig(waveConfig);
            var spawnTime = waveConfig.GetTimeBetweenSpawns() + Random.Range(0, waveConfig.GetSpawnRandomFactor());
            yield return new WaitForSeconds(spawnTime);
        }
    }
}
