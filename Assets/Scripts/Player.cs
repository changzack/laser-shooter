﻿using UnityEngine;

public class Player : MonoBehaviour
{
    // Configuration parameters
    [Header("Player Movement")]
    [SerializeField] private float moveSpeed = 10f;
    [SerializeField] private float padding = 1f;
    
    // Fields
    private float minX;
    private float maxX;
    private float minY;
    private float maxY;
    private IShooting shooting;

    private void Start()
    {
        shooting = GetComponent<IShooting>();
        SetupMoveBoundaries();
    }

    private void SetupMoveBoundaries()
    {
        Camera gameCamera = Camera.main;
        var bottomLeft = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0));
        var topRight = gameCamera.ViewportToWorldPoint(new Vector3(1, 1, 0));
        minX = bottomLeft.x + padding;
        maxX = topRight.x - padding;
        minY = bottomLeft.y + padding;
        maxY = topRight.y - padding;
    }

    private void Update()
    {
        Move();
        shooting.Fire();
    }

    private void Move()
    {
        var pos = transform.position;
        var inputX = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed;
        var inputY = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;
        var newX = Mathf.Clamp(pos.x + inputX, minX, maxX);
        var newY = Mathf.Clamp(pos.y + inputY, minY, maxY);
        transform.position = new Vector2(newX, newY);
    }
}
