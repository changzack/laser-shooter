﻿using UnityEngine;

public class EnemyShooting : MonoBehaviour, IShooting
{
    [SerializeField] private GameObject enemyLaserPrefab;
    [SerializeField] private float shootCountdown;
    [SerializeField] private float minTimeBetweenShoot = 0.5f;
    [SerializeField] private float maxTimeBetweenShoot = 1f;
    [SerializeField] private float shootSpeed = 8f;
    [SerializeField] private AudioClip shootSound;
    [SerializeField] [Range(0, 1)] private float shootVolume = 0.25f;
    
    private void Start()
    {
        Debug.Assert(Camera.main != null, "Camera.main != null");
        ResetShootCountdown();
    }
    
    public void Fire()
    {
        shootCountdown -= Time.deltaTime;
        if (shootCountdown <= 0)
        {
            MakeLaserAndSound();
            ResetShootCountdown();
        }
    }
    
    private void MakeLaserAndSound()
    {
        var laser = Instantiate(enemyLaserPrefab, transform.position, Quaternion.identity);
        laser.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -shootSpeed);
        AudioSource.PlayClipAtPoint(shootSound, Camera.main.transform.position, shootVolume);
    }
    
    private void ResetShootCountdown()
    {
        shootCountdown = Random.Range(minTimeBetweenShoot, maxTimeBetweenShoot);
    }
    
}